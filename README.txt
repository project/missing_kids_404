-- SUMMARY --

This module is written by me, Andrew Larcombe but based on an idea
and implemention by Philip Tellis described at 
http://tech.bluesmoon.info/2010/02/missing-kids-on-your-404-page.html

The idea is simple - publicise, and hopefully help find, missing children
by displaying photos and information on your sites 404 page. 

The data on missing children comes from the missingkidsmap.com website
which covers the USA, Canada and Great Britain. 

This module uses the GeoIP service at http://geoip.pidgets.com/ to
work out which region the client is in, then makes a request to 
missingkidsmap.com via Yahoo's YQL service. For added karma results for
each region are cached on the server for two hours. 

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/missing_kids_404


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

There are no configuration options in this release.